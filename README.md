# Introduction

Build [Linux from Scratch (LFS) 9.1-systemd](http://www.linuxfromscratch.org/lfs/view/9.1-systemd/) in 
[docker containers](https://docs.docker.com/), which are configured with 
[Ansible](https://docs.ansible.com/ansible/latest/index.html). Utilize [GitLab CI](https://docs.gitlab.com/ee/ci/) to
automate the whole process. Produce a bootable live-cd image and LFS system docker image.

Project was originally created for educational purposes in private [CERN](https://cern.ch/) GitLab repository. It
strives to show an example of how an LFS build can be fully automated with a modern CI system.

Refer to the LFS book to gain a better understanding of the LFS system.

# Overview of projects

The group contains the following projects, in build order:

- base toolchain - build base docker image, which serves as a base for building the toolchain.
- toolchain - build toolchain docker image, which includes programs required for building the LFS system.
- lfs-system - build LFS docker image.
- base iso - build base docker image, which includes programs required for building an LFS ISO.
- lfs-iso - build a bootable live-cd of LFS system.
- gitlab-runner-helm - umbrella helm chart that installs gitlab-runner with TLS enabled.

# Requirements

- [docker](https://www.docker.com/), tested on versions `19.03.11` and `19.03.13` 
- Linux Kernel, at least version `3.2`. See 
[LFS host requirements](http://www.linuxfromscratch.org/lfs/view/9.1-systemd/chapter02/hostreqs.html) for details.
    - While the host requirements are largely decoupled through usage of docker containers, the host's kernel is still
      shared between the host and containers.
    - Docker [requires](https://docs.docker.com/engine/install/binaries/) version `3.10` or higher of the Linux kernel,
      so this requirement should be satisfied by default.

## Host configuration

The host's configuration may still influence the build.
 
For example, consider the following CI job
[logs](https://gitlab.com/automated-linux-from-scratch/lfs-system/-/jobs/879365044) that signify an issue with
testing `perl-5.30.1`. Among the sea of red text, you can find this line:
```shell script
dist/Net-Ping/t/501_ping_icmpv6 ................................ FAILED--no leader found
```

In this case, the build machine's [ipv6 stack was disabled](https://wiki.archlinux.org/index.php/IPv6#Disable_IPv6),
which caused the failure. If you're planning to run tests, configure a working ipv6 stack.

Apart from that, consider using `overlay2` as docker's
[storage driver](https://docs.docker.com/storage/storagedriver/select-storage-driver/):
```shell script
$ docker info | grep "Storage Driver"
 Storage Driver: overlay2
```

Other drivers may work, but were never tested.

# Build

The projects can either be built:
- locally, by building with [docker](https://docs.docker.com/engine/reference/commandline/build/);
- in GitLab CI, through [GitLab runner](https://docs.gitlab.com/runner/).
    - You can choose from a number of
     [executors](https://docs.gitlab.com/runner/executors/README.html#i-am-not-sure) to run the builds. The builds were
     tested with a [docker executor](https://docs.gitlab.com/runner/executors/docker.html) and a 
     [kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html). 

## A note about CPUs and cores

You can limit CPU usage (compilation, ISO build) through `CPUS` variable. It should be constrained like
so: 
`1 <= CPUS <= MAXIMUM_CPUS`.
Be careful with setting it to maximum value, since some of the operations are very CPU
intensive, e.g. compilation of `gcc`. Be conservative if you want to keep using the build machine for other
operations - of course, this will result in overall longer build time. 

You can check how many CPUs your machine has through `lscpu`: 
```shell script
$ lscpu
CPU(s):                          8
On-line CPU(s) list:             0-7
Thread(s) per core:              2
Core(s) per socket:              4
Socket(s):                       1
```

In this case, the build machine has `1 Socket` with `4 Cores`, `2 Threads` each, which result in `8 CPUs`.

Keep in mind the distinction between the CPUs and cores if using a kubernetes executor, since it generally expects a
number of cores (not CPUs) when limiting resources. See 
[k8s docs](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#meaning-of-cpu) for more
information.

## Build with GitLab CI

Fork build projects into a group you own. At the group level, set variables:

```shell script
DOCKER_HOST=tcp://__REDACTED__:2376
DOCKER_CERT_PATH=/certs/client
DOCKER_TLS_VERIFY=1
DOCKER_VERSION=xxx
DOCKER_VERSION_QA=__REDACTED__
CPUS=__REDACTED__
```

Replace `__REDACTED__`:
- `DOCKER_HOST` - set to `tcp://docker:2376` if using docker executor or `tcp://localhost:2376` if using kubernetes
executor. This is due to how kubernetes pods are designed. From 
[kubernetes docs](https://kubernetes.io/docs/concepts/workloads/pods/):
>A Pod models an application-specific "logical host": it contains one or more application containers
>which are relatively tightly coupled. In non-cloud contexts, applications executed on the same physical
>or virtual machine are analogous to cloud applications executed on the same logical host.
- `DOCKER_VERSION` - since builds were tested with `19.03.11` and `19.03.13`, set to at least `19.03.11`.
- `DOCKER_VERSION_QA` - set it you want to run a QA build that targets a `qa` branch in each project. In that case,
be sure to create `qa` branches before running the QA pipeline.

Next step is configuring and registering your executor.

### Kubernetes executor

Refer to the `gitlab-runner-helm` project on how to set it up locally.

### Docker executor

Refer to gitlab documentation on how to [install](https://docs.gitlab.com/runner/install/docker.html) the runner and
[register](https://docs.gitlab.com/runner/register/#docker) it.

Afterwards, configure `config.TOML` to enable TLS. You can access the docker volume directly, e.g:
```shell script
$ sudo vim /var/lib/docker/volumes/gitlab-runner-config/_data/config.toml
[[runners]]
  [runners.docker]
    privileged = true
    volumes = ["/certs/client", "/cache"]
```

Ensure `privileged` and `volumes` config is the same as above.

The container should reload the config automatically. Check the logs in case of issues, e.g:
```shell script
$ docker logs -f gitlab-runner
Configuration loaded                                builds=0
listen_address not defined, metrics & debug endpoints disabled  builds=0
[session_server].listen_address not defined, session endpoints disabled  builds=0
[...]
```

## Build locally

In general, refer to `.gitlab-ci.yml` in each of the projects. It outlines the build steps.
 
Since GitLab CI is not used while building locally, there are a few differences:

- GitLab CI builds docker images within docker containers by using [docker-in-docker](https://hub.docker.com/_/docker)
 (`dind`). This approach shouldn't be necessary while building locally. Execute docker builds directly from your
  system. If you still want to set up `dind`, refer to sections below.

- GitLab CI automatically pulls the repository into docker containers. To achieve the same result, you can bind mount
 project directory into the container. This is only required during building live-cd. Example below assumes you're
 already in the project working directory - notice `-v` parameter:
```shell script
$ docker run -it --rm -v $(pwd):/repo image:tag
```

- GitLab CI automatically supplies variables and executes `before_script`. These variables are used as arguments
 while building docker images. If you don't like default values, you will have to overwrite them. For example:
```shell script
$ export CPUS=8 #overwrites group variable
$ export ROOT_PASS=my-custom-password
``` 

- There is no need to push/pull from the GitLab registry, since in this case all images are stored locally.

### How to setup `dind`?

These steps are not be required for building locally. Refer to instructions below if you want to resemble CI pipeline
as closely as possible, or for learning purposes. 
 
Create network `lfs-net`, so that containers that are connected to it can communicate via names.
```shell script
$ docker network create lfs-net
```

Start `dind` container with the following settings:
- Enable `privileged` mode. This is mandatory for running `dind`.
- Connect it to the `lfs-net` network.
- Assign it a name `docker`. Name has to be exactly `docker`. Otherwise name resolution will fail.
- Bind mount certificates generated by the docker deamon that starts in `dind` container. These certificates will be
  used by containers on the `lfs-net` that will be communicating with the docker deamon.
- Bind mount docker data into `build` directory. This ensures it will be preserved if the `dind` container is
  removed. It separates `dind` docker data from your default `docker` data in your host system. Do not bind mount
  `/var/lib/docker` from your host system into the container, or you risk data corruption. Refer to this [blog
  post](http://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/) for more information.
- Use the same image tag for `dind` as defined in GitLab CI.
```shell script
docker run -itd \
  --privileged \
  --network lfs-net \
  --name docker \
  -v $(pwd)/build/certs:/certs/client \
  -v $(pwd)/build/var-lib-docker:/var/lib/docker \
  docker:xxx-dind
```

Test if you can connect to docker deamon:
- Use basic `docker` image that has the same version as `dind`.
- Export variables that will configure docker client, so that it can communicate with docker deamon running in
 `docker` container (referring by name) via TLS.
```shell script
$ docker run -it --rm \
  --network lfs-net \
  -v $(pwd)/build/certs:/certs/client \
  docker:xxx
# export DOCKER_HOST=tcp://docker:2376
# export DOCKER_CERT_PATH=/certs/client
# export DOCKER_TLS_VERIFY=1 
# docker version
Client: Docker Engine - Community
 Version:           19.03.11
 API version:       1.40
 Go version:        go1.13.10
 Git commit:        42e35e61f3
 Built:             Mon Jun  1 09:09:53 2020
 OS/Arch:           linux/amd64 Experimental:      false

Server: Docker Engine - Community
 Engine:
  Version:          19.03.11
  API version:      1.40 (minimum version 1.12)
  Go version:       go1.13.10
  Git commit:       42e35e61f3
  Built:            Mon Jun  1 09:16:24 2020
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:  Version:          v1.2.13
  GitCommit:        7ad184331fa3e55e52b890ea95e65ba581ae3429 runc:
  Version:          1.0.0-rc10
  GitCommit:        dc9208a3303feef5b3839f4323d9beb36df0a9dd
 docker-init:  Version:          0.18.0
  GitCommit:        fec3683
```

Output should show the same version for the docker client and server.

### How to build LFS with `dind`?

Depending on the project, start the docker container from image defined in CI job.
- Connect it to `lfs-net` network.
- Bind mount project repository.
- Bind mount generated docker deamon certificates.
```shell script
$ docker run -it \
  --name my-container \
  --network lfs-net \
  -v $(pwd):/repo \
  -v $(pwd)/build/certs:/certs/client \
  job:image
```

Within the container:
- Define docker variables.
- Define CI job variables if they exist.
- Build the target image.

For example, building toolchain:
```shell script
$ export DOCKER_HOST=tcp://docker:2376
$ export DOCKER_CERT_PATH=/certs/client
$ export DOCKER_TLS_VERIFY=1
$ export BASE_TOOLCHAIN_REGISTRY_IMAGE=base-toolchain
$ export BASE_TOOLCHAIN_TAG=dev
$ export CPUS=4
$ docker build -t toolchain:dev -f /repo/Dockerfile \
      --build-arg BASE_TOOLCHAIN_REGISTRY_IMAGE \
      --build-arg BASE_TOOLCHAIN_TAG \
      --build-arg CPUS \
      /repo/ansible
```

If you set up `dind` as described in the section above, the image will be saved in `build/var-lib-docker`. It can
be referenced in next build steps.

# Development, debugging and idempotence

Imagine the toolchain build fails at downloading one of the sources. If you start the docker build again, it will
start a new build container, pulling all sources from scratch again. You can prevent that by committing the failed
build container as new image:
```shell script
# find out the id or name of the failed build container
$ docker ps -a
# commit it as new image
$ docker commit ${id} toolchain:dev
# build from a new image, e.g:
$ cat Dockerfle
FROM toolchain:dev as builder
[...]
```

Another example is when building a package fails. Let's assume building `gcc` has failed. In order to investigate the
reason, you could commit the failed container as explained above, and start it. Then, you could build the package
manually to fully inspect the output.
```shell script
$ docker run -it --rm toolchain:dev /bin/bash
$ cd /build/sources/gcc-9.2.0
# build the package as instructed in ansible
```

Depending on the reason, you may have to start the build from the beginning, e.g. if the package that `gcc` depends on
was built incorrectly. Otherwise, after fixing package build in ansible, you may be able to comment out all ansible
tasks that take place before building `gcc` and start the next build from that image. This speeds up testing the
build. It's still a good idea to build an image from zero after you're done with testing/development.

Regarding idempotence, playbooks may fail if executed multiple times. For example, executing ansible playbook
`00_build_temporary_system.yml` from the already built image will result in permission failure, since the 
final step in that playbook is changing permissions for `/tools` to `root`.

A cleanup may be required in some cases. For example, when a playbook is stopped  halfway, it may be necessary to 
remove unpacked directory of a package if it was in the middle of compilation.

To start the container with a possibility of interacting with it (notice `-i` parameter):
```shell script
$ docker run -itd --name container-name image:tag
``` 

To execute into the container:
```shell script
$ docker exec -it container-name /path/to/shell
```

In general, you will have to refer to both LFS book and project files to debug issues. 

# Major differences to LFS book

- Build process is largely separated from the host system by using docker containers.
- Build process is automated with GitLab CI and ansible.
- Final results are a bootable live-cd iso image and an LFS docker image.
- `zlib` package has to be installed before `python` while constructing toolchain. Otherwise, `python` 
compilation will fail to build `binascii` module, that is required when running ansible during next playbooks.
- `ansible` package is installed as part of the toolchain. It requires additional python modules, `openssl` and
 `libffi` packages to be installed.
- Linux kernel is compiled with support for squashfs and overlayfs in order to build a functional live-cd.
- [System configuration (chapter 7)](http://www.linuxfromscratch.org/lfs/view/9.1-systemd/chapter07/introduction.html)
is mostly left out, since it's not necessary for creating a live-cd. It should be applied on an actual non-live system.

# Example improvements

While the build is stable and produces desired results, there is room for improvement. For example:

- Some tests are not executed, since they are failing. While this is expected for some packages, the test results should
be inspected and understood. `gcc` test saves its results for further analysis in `/test-results` directory. This
could be applied for each failing test. Test results could also be parsed within ansible to decide whether they
should be considered as passing.
- Ansible tasks contain multiple `shell` modules which could be replaced by native ansible modules.
- Playbooks could be designed to be idempotent.
